<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Setting (SettingController)
 * Setting Class to control all user setting related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Setting extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('setting_model');
        $this->isLoggedIn();  
		
    }
    
    /**
     * This function used to load the first screen of the user
     */
   
    function index()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('setting_model');
        
            
            $this->load->library('pagination');
            
            $count = $this->setting_model->settingUserCount();
		

			$returns = $this->paginationCompress ( "setting/index", $count, 500 );
            
            $data['userRecords'] = $this->setting_model->settingListing($returns["page"], $returns["segment"]);
			
			$this->global['selected'] = 'Edit User';
            $this->global['pageTitle'] = 'Setting User Listing';
            
            $this->loadViews("setting/index", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the edit form
     */
    function edit()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
			$id_user = $this->input->post("iduser");
            $this->load->model('setting_model');
			$data['fields'] = $this->setting_model->settingColumns();
			$data['akses'] = $this->setting_model->fieldUser($id_user);
           
            $this->global['pageTitle'] = 'Edit Privileges User';
			$this->global['selected'] = 'Edit User';
            $this->loadViews("setting/edit", $this->global, $data, NULL);
        }
    }
	
	function password()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('setting_model');
			$data['users'] = $this->setting_model->allUser();			
           
            $this->global['pageTitle'] = 'Edit Password User';
			$this->global['selected'] = 'Password';
            $this->loadViews("setting/password", $this->global, $data, NULL);
        }
    }
	
	function copyPrivileges()
    {
		$this->global['selected'] = 'Privileges';
		
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('setting_model');
			$data['users'] = $this->setting_model->allUser();			
           
            $this->global['pageTitle'] = 'Copy Privileges User';
			
            $this->loadViews("setting/copy", $this->global, $data, NULL);
        }
    }

    
    /**
     * This function is used to add new user to the system
     */
    function store()
    {
		
		
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {	
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('iduser','ID User','trim|required|max_length[128]|xss_clean');
           
            
            if($this->form_validation->run() == FALSE)
            {
                $this->create();
            }
            else
            {
				$this->load->model('setting_model');
                $iduser = (($this->input->post('iduser')));
				
				
				
				
             
              
                $userInfo = array();
				
				$fields = $this->setting_model->settingColumns();
				
				foreach($fields as $f){
					$name = $f->COLUMN_NAME;
					if (isset($_POST['akses_' . $name]))
					{						
						$userInfo[$name] = "true";
					}
					else{
						$userInfo[$name] = "false";
					}
					
				}
				
		
                
                
                $result = $this->setting_model->editUser($userInfo, $iduser, ENC_USER);
				
                $this->session->set_flashdata('success', 'User edit successfully');
                
                redirect('setting/index');
            }
        }
    }
	
	function _startsWith($haystack, $needle)
	{
		 $length = strlen($needle);
		 return (substr($haystack, 0, $length) === $needle);
	}

     function storePassword()
    {
		
		
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {	
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('password','Password','trim|required|max_length[128]|xss_clean');
           
            
            if($this->form_validation->run() == FALSE)
            {
                $this->create();
            }
            else
            {
				$this->load->model('setting_model');
                $password = $this->input->post('password');
				
				
				
				
             
              
                $userInfo = array();
				$userinfo["password"] = $password;
				$do = false;
				foreach($_POST as $key=>$value)
				{
					
					if ($this->_startsWith($key, "user") > 0)
					{
						$iduser = $value;
						$result = $this->setting_model->editPassword($password, $iduser, ENC_USER, ENC_PWD);
						$do = true;
					}
				}				
				
			
                
                
                
				if($do) {
					$this->session->set_flashdata('success', 'Password edit successfully');
				}
				else{
					$this->session->set_flashdata('error', 'Password edit fail, Please select at least 1 User');
				}
                
                redirect('setting/password');
            }
        }
    }
    
    
	function storePrivileges()
    {
		
		
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {	
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('from','From User','trim|required|max_length[128]|xss_clean');
           
            
            if($this->form_validation->run() == FALSE)
            {
                $this->create();
            }
            else
            {
				$this->load->model('setting_model');
                $from = $this->input->post('from');
              
				$do = false;
				foreach($_POST as $key=>$value)
				{
					
					if ($this->_startsWith($key, "user") > 0)
					{
						$to = $value;
						$result = $this->setting_model->copyPrivileges($from, $to, ENC_USER);
						$do = true;
					}
				}				
				
			
                
                
                
				if($do) {
					$this->session->set_flashdata('success', 'Copy Privileges successfully');
				}
				else{
					$this->session->set_flashdata('error', 'Copy Privileges fail, Please select at least 1 User');
				}
                
                redirect('setting/copyPrivileges');
            }
        }
    }
  
	
	
}

?>