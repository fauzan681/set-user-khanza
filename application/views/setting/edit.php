<?php
foreach($akses as $a){?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <small>Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addUser" action="<?php echo base_url() ?>setting/store" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Id User</label>
                                        <input type="text" value="<?php echo $a['id_usr'];?>" disabled class="form-control required" id="iduser" name="iduser" maxlength="128">
                                    </div>
                                    
                                </div>
                               
                            </div>
							<input type="hidden" name="iduser" value="<?php echo $a['id_usr'];?>"/>
                            
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Akses</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($fields as $f) { 
												$desc = $f->COLUMN_COMMENT;
												$checked = "";
												if(empty($desc))
												{
													$desc = ucfirst(str_replace("_", " ", $f->COLUMN_NAME));
												}
												if($a[$f->COLUMN_NAME] == "true")
												{
													$checked = "checked";
												}
											?>
												<tr>
													<td><input <?php echo $checked;?> value="true" type="checkbox" name="akses_<?php echo $f->COLUMN_NAME;?>"/> <?php echo $desc;?>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
                           
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<?php } ?>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>